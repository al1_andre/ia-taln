class HomesController < ApplicationController

  # GET /homes
  # GET /homes.json
  def index
    @manager = Ai::Nlp::Languages.new
    @languages = @manager.all
    interpret_input
  end

  private
  
    ##
    # Interprète le texte fourni
    def interpret_input
      return if params['input'].nil?
      byebug
      guess = @manager.guess(params['input'])
      guess 
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def home_params
      params.permit(:input)
    end
end
