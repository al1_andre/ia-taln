
# Objet
J'entends de plus en plus parler de l'**intelligence artificielle** et des avancées sidérantes des **GAFA** (Google, Apple, Facebook, Amazon) et autres **NATU** (Netflix, Airbnb, Tesla et Uber) dans ce domaine et ça m’intéresse beaucoup. Je souhaite en **apprendre** plus mais si l'Internet est plein d'informations, il faut tout de même savoir la chercher. Il y a de nombreux articles en **Anglais**, peu en **Français** alors que les concepts ne sont pas simple de prime abord. Je souhaite donc apporter ma pierre à l'édifice en **partageant** ce que j'apprends. C'est un projet à but non lucratif, ouvert à tous sous licence MIT dont le seul but est l'**apprentissage** par l'**expérience**.

# Description
> On regroupe habituellement sous le terme d'intelligence artificielle un ensemble de notions s'inspirant de la cognition humaine ou du cerveau biologique, et destiné s à assister ou suppléer l'individu dans le traitement des informations massives.

En janvier **2017**, le **secrétaire d’État chargé de l'Industrie, du Numérique et de l'Innovation** et le **secrétaire d’État chargé de l'Enseignement supérieur et de la Recherche** ont lancé la démarche **#franceIA** qui avait pour objectif d'étudier les opportunités de l'**IA**. Cette étude se structure autour de **trois piliers** qui ont été déclinés en **dix thèmes** clés et traités par dix **groupes de travail** et sept sous-groupes. Leur rapport de synthèse est disponible [ici](https://www.economie.gouv.fr/files/files/PDF/2017/Rapport_synthese_France_IA_.pdf).

L'intelligence artificielle traite les sujets suivants :

||||
|:----|:----|:----|
|La résolution des problèmes | La [représentation des connaissances](https://fr.wikipedia.org/wiki/Repr%C3%A9sentation_des_connaissances) | L'[organisation](https://fr.wikipedia.org/wiki/Planification_(intelligence_artificielle)) |
|L'[apprentissage](https://fr.wikipedia.org/wiki/Apprentissage_automatique) | La [compréhension du langage humain (NLP/TALN)](https://fr.wikipedia.org/wiki/Traitement_automatique_du_langage_naturel) | La [perception machine](https://fr.wikipedia.org/wiki/Machine_perception) |
| La [robotique](https://fr.wikipedia.org/wiki/Robotique) | L'[information affective](https://fr.wikipedia.org/wiki/Informatique_affective) | La [créativité](https://en.wikipedia.org/wiki/Computational_creativity) |
| La [conscience de soi](https://fr.wikipedia.org/wiki/Intelligence_artificielle#Intelligence_artificielle_forte) | | |


## Pourquoi un tutoriel en français ?
Il ne faut pas se **mentir**, la langue **universelle** de la **technique** est bel et bien l'**anglais** aujourd'hui. Mon code est en anglais ainsi que ses commentaires. C'est une **obligation** si l'on veut pouvoir **partager** avec le plus grand monde possible ; étant un grand défenseur de l'**open-source**, je suis persuadé que le partage du code est ce qui le rend plus fort, plus **sûr**.

Je ne dirais pas comme le **Docteur Laurent Alexandre**, dont le livre [La Guerre des intelligences](https://www.mollat.com/livres/2088679/laurent-alexandre-la-guerre-des-intelligences-comment-l-intelligence-artificielle-va-revolutionner-l-education) que la France a déjà perdu la guerre de l'IA. Je suis plutôt de l'avis de [Cédric Villani](https://fr.wikipedia.org/wiki/C%C3%A9dric_Villani). Si les **Français** se trouvent assez **bien représentés** en ce qui touche à l'**I**ntelligence **A**rtificielle comme le dit le rapport du gouvernement Hollande, les **articles** en Français sur Internet sont vieux, vide de sens des fois, trop **compliqués** souvent ; mais surtout **jamais sexy** ! L'IA semble, pour le commun des mortels, **intouchable**, **stratosphérique** et même pour certains considéré à surtout éviter, dangereux voir **anti-humain** ! Hors il y a un **enjeu** national de taille. Nous avons **raté** le passage à l'**Internet** parce que nous pensions être les **maitres du monde** avec notre **minitel** que nous n'avons pas fait évoluer ; il s'agit là de ne pas manquer la marche -marche déjà bien grandie par des années d'inaction- en pensant que fermer les yeux nous évitera d'être dominés par une IA !

Une chose extremement importante à comprendre dans le **fonctionnement** de l'IA, c'est que le **code** n'est pas le plus important aspect de cette intelligence ; c'est la **donnée** qui la constitue ! C'est là une barriere conseptuelle importante car il s'agit de quasi l'inverse des programmes que tout un chacun connait. [AlphaGo](https://fr.wikipedia.org/wiki/AlphaGo) de [DeepMind](https://deepmind.com/) par exemple n'a pas gagné contre champion du monde [Ke Jie](https://fr.wikipedia.org/wiki/Ke_Jie) en calculant toutes les possibilités de coups (un algorithme), mais en se basant sur son experience acquise (les données) comme un joueur l'aurait fait.

Et si à l'heure où j'écris, les **infrastructures** françaises sont à la ramasse (quasi **inaccessibles**), nos **ingénieurs** se **vendent** bien ; mais pas chez nous ! Si nous étudions l'IA, si nous en parlons ; si nous écrivons des articles dessus, c'est en **anglais** ! Hors dans les enjeux de l'IA, on compte l'**économie**, la **santé**, l'**industrie** qui ont des dimensions **humaines** fortes ; si on **alimente** les IA avec seulement des **données** et des **concepts** en **anglais**, on peut oublier le mode de **pensée à la française** qui pourtant historiquement a une bonne réputation.

Et l'IA va rapidement **devenir** un outil d'**aide** à la prise de décision dans les **entreprises**, dans la **société** ainsi que dans notre quotidien. Enfait, c'est **déjà** le cas pour vos **recherches** sur Internet, vos **itinéraires** en voitures, la gestion de **vos données** sur les **clouds** etc. Lorsque **vous** répondez à un captcha afin de savoir si vous êtes bien un **humain**, vous pensez vraiment que **Google** ne vous **utilise** pas pour **former** une IA ? Si **Skynet** vous dit quelque chose, je vous laisse comprendre l'**importance** d'une **indépendance** de l'IA ou tout du moins d'une IA **alliée** (française ou européenne).

# Mon idée
Comme le soulève le **rapport de synthèse de France IA** de 2017, la France **manque** cruellement de **tutoriels** et de cours abordables en ce qui concerne l'IA ; mon **idée** est d'en **créer** un qui soit **reproduisible**, **accessible**, **ludique** et **agréable**.

Si j'ai appris une chose au fil de mes années d’**expériences** de développeur, c'est qu'il ne faut pas **réinventer** la roue ! Si on a une idée, quelqu'un dans le monde l'a très probablement **déjà eu**, et il vaut mieux **réutiliser** son travail. La **difficulté** est de le trouver et d'en avoir les **droits**. Par exemple, **Facebook** met à disposition des outils pour jouer avec son IA [wit](https://wit.ai/) qui permet, entre autre, de créer des chat-bot mais l'**IA** et ses **données** se trouve **chez eux**. Autrement dit, pendant que **vous entrainez** votre bot, vous entrainez surtout **le leur**. Il n'y a à ma connaissance que l'[OpenIA](https://openai.com/) d'[Elon Musk](https://fr.wikipedia.org/wiki/Elon_Musk) qui se veut universelle et pour l'**humanité** plus que pour l'entreprise et le **profit**. Si le profit est bon, il ne doit être qu'un **résultat** et non un **objectif** !

Je pense donc me baser sur des **concepts**, rechercher du code les gérants plus ou moins et tenter de recréer une IA **disponible** pour **tous** (disponible sous la gem [ai-nlp](https://rubygems.org/gems/ai-nlp)).

Je dois faire un choix sur le type d'IA que je souhaite créer. Je suis un **littéraire**, et la plupart des cours et informations que je trouve sur l'IA sont basés sur des **mathématiques** ; alors j'ai décidé d'accer mon IA sur les éléments suivants :

 - L'[apprentissage](https://fr.wikipedia.org/wiki/Apprentissage_automatique)
 - La [compréhension du langage humain (NLP/TALN)](https://fr.wikipedia.org/wiki/Traitement_automatique_du_langage_naturel)

Cette IA doit donc **apprendre** à **comprendre** un **texte humain**. Une interface web doit **afficher** de manière **graphique** la somme de **connaissances** qu'elle acquière, contenir une zone permettant à l'humain de **communiquer** avec l'IA et une zone dans laquelle elle **répond**.

# Technologies et concepts
Au tout début, mon IA ne **comprendra** rien à rien ! Il va falloir mettre en place un système d'**apprentissage** ! 

Mais qu'est-ce que l'apprentissage ? Il s'agit d'**analyser**, du **regrouper**, de **stocker** l'information afin de pouvoir l'**exploiter**.

## L'Analyse
> L’analyse sémantique représente l’ensemble des procédés visant à analyser le sens des mots et des phrases, elle est le plus souvent utilisée comme préambule au traitement automatique des langues.

Si je veux que l'IA comprenne le langage humain, je dois tout d'abord trouver comment réaliser les actions suivantes :
 - Déterminer la langue (cf [N-gramme](https://fr.wikipedia.org/wiki/N-gramme))
 - Déterminer la [syntaxe](https://fr.wikipedia.org/wiki/Analyse_syntaxique)
 - Découper les éléments de la phrase [entités lexicales](https://fr.wikipedia.org/wiki/Analyse_lexicale) (ou tokens en anglais)

### Le langage informatique
Je suis un grand fan de [Ruby](https://www.ruby-lang.org/fr/), c'est un langage dont la philosophie est basée sur le [paradigme objet](https://fr.wikipedia.org/wiki/Smalltalk) ; il est donc réflexif et dynamiquement typé. je pense qu'il est capable d'apporter des avantages à mon projet grâce à sa [réflexivité](https://fr.wikipedia.org/wiki/R%C3%A9flexion_(informatique)) ; c'est à dire qu'un programme Ruby est capable d'examiner, et éventuellement modifier, ses propres structures internes de haut niveau lors de son exécution !

Il y a, à mon avis, une belle opportunité à **exploiter** pour tout ce qui concerne les [lemmes](https://fr.wikipedia.org/wiki/Lemme_(linguistique)).

## Le Regroupement 
> Le clustering : Il s'agit, pour un logiciel, de diviser un groupe hétérogène de données, en sous-groupes de manière que les données considérées comme les plus similaires soient associées au sein d'un groupe homogène et qu'au contraire les données considérées comme différentes se retrouvent dans d'autres groupes distincts ; l'objectif étant de permettre une extraction de connaissance organisée à partir de ces données.

Une fois mes éléments de phrase correctement **identifiés**, je dois réussir à les **regrouper**. Il est possible de les regrouper par sens (homonymes, synonymes, racines, préfixes, suffixes, lemmes) par type (nom, verbe, adjectif, etc.)

Ce regroupement est donc **crucial**, c'est lui qui va me permettre de **stocker** au bon **endroit** mes données. Je dois être capable de faire ces regroupements de façon **non supervisés** dans certains cas et **supervisé** dans d'autres.

### Non supervisés
Je peux donc automatiquement regrouper, classifier des éléments que j'ai découpé à l'aide de ma base de données. Mais si par exemple aucune donnée ne me permet de faire un regroupement non supervisé, je dois interagir avec l'utilisateur ; je dois lui demander de m'aider à classer cet élément.

### Supervisés
L'IA doit être en mesure d’interagir avec l'utilisateur et de lui demander de l'aide pour classifier les éléments qu'il aura découpé. 

L'utilisateur doit permettre de préciser :
 - le type
 - le sens
 - l'origine (lemme)

## Le stockage
Le stockage doit permettre une récupération des données qui permette une bonne exploitation de ces dernières. 

## L'exploitation
Tout ceci est magnifique, mais si l'IA arrive maintenant à **découper** et faire des **regroupements** d'information, il lui faut pourvoir les **exploiter**. L'avantage d'un **moteur de recherche** comme **QWANT** ou **Google**, par exemple, c'est qu'il sait que sa seule **action** est de **fournir des données** qu'il a collecté et regroupé à la **demande** ; il n'a pas vraiment besoin de **comprendre** ce qu'il stocke.

Une IA plus évoluée comme **SIRI** par contre, est censée **répondre** à diverses **actions** comme **ajouter** un évènements à l'agenda, **donner** le temps qu'il fera **demain** ou encore **trouver** le **chemin** d'un magasin **proche** de notre position. Il faut donc qu'il **comprenne** le **type de demande** qui est faite.

# Références 
 - [Rapport France-IA](https://www.economie.gouv.fr/France-IA-intelligence-artificielle#L%27IA)
 - https://github.com/sjchoi86/Deep-Learning-101?files=1
 - https://speakerdeck.com/geoffreylitt/deep-learning-an-introduction-for-ruby-developers
 - https://github.com/andrewt3000/DL4NLP/blob/master/README.md

## Deep Learning
 - [wiki](https://fr.wikipedia.org/wiki/Apprentissage_profond)
 - [Le monde](http://www.lemonde.fr/pixels/article/2015/07/24/comment-le-deep-learning-revolutionne-l-intelligence-artificielle_4695929_4408996.html)
 - [ruby introduction](https://speakerdeck.com/geoffreylitt/deep-learning-an-introduction-for-ruby-developers)

## NLP 
 - [TAL](http://blog.onyme.com/traitement-automatique-des-langues-tal-intelligence-artificielle-ia-analyse-semantique-et-clusterings/)
 - [Lemmatisation](http://blog.onyme.com/lemmatisation-et-racinisation-en-francais-flexion-lemme-et-racine-dun-mot/)
 - [wiki](https://en.wikipedia.org/wiki/Natural_language_processing)
 - [Structure de la phrase en Français](http://la-conjugaison.nouvelobs.com/regles/grammaire/les-structures-de-phrases-167.php)
 - [lemmatizer](https://github.com/yohasebe/lemmatizer)

## Programmes/API
 - [Facebook wit](https://wit.ai/)
 - [Deeptext](https://humanoides.fr/deeptext-la-nouvelle-intelligence-artificielle-de-facebook-pour-decortiquer-la-semantique/)

## Ruby
 - [AI4R](https://github.com/SergioFierens/ai4r/)
 - [rubynlp](http://rubynlp.org/)
 